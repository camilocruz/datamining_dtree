import java.util.ArrayList;
import java.lang.StringBuilder;
import java.util.Queue;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collections;

public class DTreeC45{
    private TreeNode root;
    private ArrayList<TreeNode> nodes = new ArrayList<TreeNode>();
    private static StringBuilder sb = new StringBuilder();
    private double zScore, confidenceFactor;
    private int numMinNodes;
    private int numLabels;
    private int numPruned;
    private static HashMap<String, Integer> label2Index = null;  
    private static HashMap<Integer, String> index2Label = null;  

    public DTreeC45(ArrayList<Record> trainData, double _zScore, int _numMinNodes, HashMap<String, Integer> _label2Index, boolean prune){
        label2Index = _label2Index;
        Record.label2Index = label2Index;
        index2Label = new HashMap<Integer, String>();
        for(String key : label2Index.keySet()){
            index2Label.put(label2Index.get(key), key);
        }
        numLabels = label2Index.size();
        zScore = zScore;
        numMinNodes = _numMinNodes;

        if(Main.DEV_MODE){
            System.out.printf("Constructor of Tree, train.size=%d, CF=%f, minNode=%d\n", 
                    trainData.size(), zScore, numMinNodes);
        }
        numPruned = 0;
        confidenceFactor = 0.25;// Z=1.15
        root = train(trainData, 0);
        if(prune)            prune();
    }

    /*
     *  Build the n-way decision tree recursively.
     */
    private TreeNode train(ArrayList<Record> data, int level){
        if(Main.DEV_MODE){
            System.out.printf("\nin train, level = %d, size = %d\n", level, data.size());
        }
        if(data.size() < numMinNodes)  return null;
        int[] count = countLabel(data);
        if(Main.DEV_MODE){
            printArray(count);
        }
        TreeNode cur;
        cur = new TreeNode(level, count, zScore, confidenceFactor);
        nodes.add(cur);
        if( isPure(count) ) // this node contains pure data with same label.
            return cur;
        Condition cond = findBestCondition(data);
        if( cond == null )  // cannot split any more
            return cur;
        cur.setCondition(cond);
        if(cond.getIsDiscrete()){
            ArrayList<ArrayList<Record>> branchData = filterByDisCondition(data, cond);
            for(ArrayList<Record> branch : branchData){
                cur.addChild( train(branch, level+1) );
            }
        }
        else{
            ArrayList<Record> leftData = filterByCondition(data, cond, true);
            ArrayList<Record> rightData = filterByCondition(data, cond, false);
            cur.addChild( train(leftData, level+1) );
            cur.addChild( train(rightData, level+1) );
        }
        if(Main.DEV_MODE){
            System.out.printf("best cond = %s\n", cond);
        }
        return cur;
    }

    /*
     *  Call by outside to prune the tree.
     */
    public void prune(){
        prune(root);
    }

    /*
     *  Post traversal the tree to prune from the leaf (bottom up)
     */
    private void prune(TreeNode root){
        if(root == null)  return;
        ArrayList<TreeNode> children = root.getChildren();
        if(children.size() == 0) // leaf.
            return;
        double childError = 0.0;
        for(TreeNode child : children){
            prune(child);
            if(child != null)
                childError += child.getErrorBound();
        }
        if(childError < 1e-10) return; // all children are null;
        // cal error of this node.
        double curError = root.getErrorBound();

        // decide whether to delete all subtrees.
        if(curError < childError){
            for(TreeNode child : children)
                if(child != null){
                    child.setPrune(true);
                    numPruned ++;
                }
        }
        else
            root.setErrorBound(childError);
    }

    /*
     *  Give the prediciton for a list of Records.
     */
    public void test(ArrayList<Record> testData){
        for(Record r : testData){
            String ans = predict(r);
            System.out.println(ans + " for " + r);
        }
    }

    /*
     *  Predict the label of a Record.
     */
    public String predict(Record r){
        if(Main.DEV_MODE)
            System.out.println("\nin predict for record:" + r);
        TreeNode prev = null;
        TreeNode cur = root;

        while(cur != null){
            if(Main.DEV_MODE)
                System.out.println("achieved node:" + cur);
            if(cur.getPruned())
                return getNodeLabel(prev);
            if(cur.getCondition() == null)
                return getNodeLabel(cur);
            prev = cur;
            cur = cur.findNext(r);
        }
        return getNodeLabel(prev);
    }

    @Override
        public String toString(){
            sb.delete(0, sb.length());
            sb.append("\nPrinting Tree: ");
            sb.append(String.format("CF=%f, minNode=%d\n", 
                        zScore, numMinNodes));
            Queue<TreeNode> q = new LinkedList<TreeNode>();
            Queue<TreeNode> next = new LinkedList<TreeNode>();
            q.add(root);
            while(!q.isEmpty()){
                while(!q.isEmpty()){
                    TreeNode cur = q.poll();
                    if(cur == null){
                        sb.append("#");
                        continue;
                    }
                    sb.append(cur);
                    sb.append("\n");
                    if(cur.getLevel() == -1) continue;
                    for(TreeNode child : cur.getChildren())
                        next.add(child);
                }
                sb.append("\n");
                q = new LinkedList<TreeNode>(next);
                next = new LinkedList<TreeNode>();
            }
            sb.append(String.format("# node = %d, # pruned = %d", nodes.size(), numPruned));
            return sb.toString();
        }

    /*
     * Find the best split condition for this data.
     */
    private Condition findBestCondition(ArrayList<Record> data){
        double originH = calEntropy(countLabel(data));
        Condition bestCond = null;
        double bestGainRatio = Double.NEGATIVE_INFINITY;
        Condition curCond = null;
        double curGainRatio = Double.NEGATIVE_INFINITY;
        // Check continuous features;
        curCond = findBestConditionOfContiF(data, originH);
        if(curCond != null)
            curGainRatio = calGainRatioByContiCondition(data, curCond, originH);
        if(bestGainRatio < curGainRatio){
            bestGainRatio = curGainRatio;
            bestCond = curCond;
        }
        // Check discrete features;
        curCond = findBestConditionOfDiscreteF(data, originH);
        if(Main.DEV_MODE){
            System.out.println("in best: "+curCond);
        }
        if(curCond != null)
            curGainRatio = calGainRatioByDisCondition(data, curCond, originH);
        if(bestGainRatio < curGainRatio){
            bestGainRatio = curGainRatio;
            bestCond = curCond;
        }
        assert bestCond != null;
        return bestCond;
    }

    /*
     * Find the best splt condition for continuous features.
     */
    private Condition findBestConditionOfContiF(ArrayList<Record> data, double Hy){
        Condition bestCond = null;
        double bestGainRatio = Double.NEGATIVE_INFINITY;
        Condition curCond = null;
        double curGainRatio;
        ArrayList<Double> possibles;
        int numF = data.get(0).conFeature.size();

        for(int i = 0; i < numF; ++i){
            possibles = new ArrayList<Double>();
            // collect each possible
            for(Record r : data)
                possibles.add(r.conFeature.get(i));
            Collections.sort(possibles);
            if(Main.DEV_MODE){
                System.out.printf("i=%d: ", i);
                System.out.println("conti possibles = " + possibles);
            }

            for(int j = 1; j < possibles.size(); ++j){
                if(possibles.get(j) - possibles.get(j-1) < 1e-10) continue;
                double mid = ( possibles.get(j-1) + possibles.get(j) )/ 2.0;
                curCond = new Condition(i, mid);
                curGainRatio = calGainRatioByContiCondition(data, curCond, Hy);
                if(Main.DEV_MODE){
                    System.out.printf("i=%d, j=%d en=%f:", i, j, curGainRatio);
                    System.out.println("cond = " + curCond);
                }
                if(bestGainRatio < curGainRatio){
                    bestGainRatio = curGainRatio;
                    bestCond = curCond;
                }
            }
            if(Main.DEV_MODE){
                System.out.printf("min entropy for discrete = %f\n", bestGainRatio);
                System.out.println("best cond: " + bestCond);
            }
        }
        return bestCond;
    }

    /*
     * Find the best splt condition for category features.
     */
    private Condition findBestConditionOfDiscreteF(ArrayList<Record> data, double Hy){
        assert data.size() != 0;
        if(Main.DEV_MODE){
            System.out.println("in find cond by discrete");
            System.out.printf("data.size = %d\n", data.size());
            System.out.printf("data = %s\n", data);
        }
        Condition bestCond = null;
        double bestGainRatio = Double.NEGATIVE_INFINITY;
        Condition curCond = null;
        double curGainRatio;
        HashSet<String> possibles = null;
        int numF = data.get(0).disFeature.size();

        for(int i = 0; i < numF; ++i){
            possibles = new HashSet<String>();
            // collect each possible
            for(Record r : data)
                possibles.add(r.disFeature.get(i));
            if(Main.DEV_MODE){
                System.out.println("possibles = " + possibles);
            }
            if(possibles.size() == 1) continue; // cannot split by this feature

            curCond = new Condition(i, new ArrayList<String>(possibles));
            curGainRatio = calGainRatioByDisCondition(data, curCond, Hy);
            if(bestGainRatio < curGainRatio){
                bestGainRatio = curGainRatio;
                bestCond = curCond;
            }
            if(Main.DEV_MODE){
                System.out.printf("min entropy for discrete = %f\n", bestGainRatio);
                System.out.println("best cond: " + bestCond);
            }
        }
        return bestCond;
    }

    /*
     * For discrete features:
     * Hy = H(Y)
     * entropy =  H(Y|X) = sum( P(X=x) * H(Y|X=x) )
     * SplitInfo = calEntropy(X) = H(X)
     */
    private double calGainRatioByDisCondition(ArrayList<Record> data, Condition condition, double Hy){
        assert data.size() != 0;
        ArrayList<ArrayList<Record>> branchData = filterByDisCondition(data, condition);
        double total = (double)data.size();
        double entropy = 0.0;
        double weight;
        int[] splitCount = new int[branchData.size()];
        int index = 0;
        for(ArrayList<Record> branch : branchData){
            weight = ((double)branch.size())/total;
            entropy += weight * calEntropy(countLabel(branch));
            splitCount[index++] = branch.size();
        }
        return (Hy - entropy) / calEntropy(splitCount);
    }

    /*
     * For continuous features:
     * Hy = H(Y)
     * entropy =  H(Y|X) = sum( P(X=x) * H(Y|X=x) )
     * SplitInfo = calEntropy(X) = H(X)
     */
    private double calGainRatioByContiCondition(ArrayList<Record> data, Condition condition, double Hy){
        assert data.size() != 0;
        ArrayList<Record> left = filterByCondition(data, condition, true);
        ArrayList<Record> right = filterByCondition(data, condition, false);
        if(Main.DEV_MODE){
            System.out.println("cal entropy by cond = " + condition);
            System.out.printf("size = %d, left = %d, right = %d\n", data.size(), left.size(), right.size());
        }
        double total = (double)data.size();
        double leftw = (double)left.size() / total;
        double rightw = (double)right.size() / total;
        double entropy = leftw * calEntropy(countLabel(left))
            + rightw * calEntropy(countLabel(right));
        return (Hy - entropy) / calEntropy(new int[]{left.size(), right.size()});
    }

    /*
     * Filter the data by given condition with discrete feature.
     * I.e., return[0] = {x where condition[0](x) = target, for all x in data};
     *       return[1] = {x where condition[1](x) = target, for all x in data};
     *       return[2] = {x where condition[2](x) = target, for all x in data};
     *       ...
     */
    private ArrayList<ArrayList<Record>> filterByDisCondition(ArrayList<Record> data, Condition condition){
        ArrayList<ArrayList<Record>> ret = new ArrayList<ArrayList<Record>>();
        for(Condition cond : condition.getChildren())
            ret.add(filterByCondition(data, cond, true));
        return ret;
    }

    /*
     * Filter the data by given condition
     * I.e., return = {x where condition(x) = target, for all x in data};
     */
    private ArrayList<Record> filterByCondition(ArrayList<Record> data, Condition condition, boolean target){
        ArrayList<Record> ret = new ArrayList<Record>();
        for(Record r : data)
            if( condition.satisfy(r) == target )    ret.add(r);
        return ret;
    }

    /*
     * Count the num of each label of this data
     */
    private int[] countLabel(ArrayList<Record> data){
        int[] count = new int[numLabels];
        for(Record r : data)
            count[r.getLabelInt()] ++;
        return count;
    }

    /*
     *  e = sum( prob * log (1/prob) )
     *    = - sum( prob * log (prob) )
     *
     *  e = zero if only one outcome.
     *  e > 0    otherwise
     *  Assume sum(count) > 0
     */
    private double calEntropy(int[] count){
        if(Main.DEV_MODE){
            System.out.println("in cal entropy");
            printArray(count);
            if(isPure(count))                 System.out.printf("entropy = 0.0\n");
        }
        if(isPure(count)) return 0.0;
        double total = 0.0;
        for(int i = 0; i < count.length; ++i) total += count[i];
        double ans = 0.0;
        double prob;
        for(int i = 0; i < count.length; ++i){
            if(count[i] == 0) continue;
            prob = (double)count[i] / total;
            ans -= prob * Math.log(prob);
        }
        if(Main.DEV_MODE){
            System.out.printf("entropy = %f\n", ans);
        }
        return ans;
    }

    /*
     *  Only contains one label.
     *  Assume sum(count) > 0
     */
    private boolean isPure(int[] count){
        boolean flag = false;
        for(int i = 0; i < count.length; ++i)
            if(count[i] > 0){
                if(flag)  return false;
                flag = true;
            }
        return true;
    }

    /*
     * print a int array
     */
    private void printArray(int[] array){
        System.out.printf("Print array: [");
        for(int i = 0; i < array.length; ++i)
            System.out.printf("%d\t", array[i]);
        System.out.printf("]\n");
    }

    /*
     * Get the label in String format.
     */
    private String getNodeLabel(TreeNode node){
        assert node != null;
        int label = node.getLabel();
        return index2Label.get(label);
    }
}
