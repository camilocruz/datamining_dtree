import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.ArrayList;
import java.lang.Double;
import java.util.Collections;
import java.util.Random;

public class Main{
    public static final boolean DEV_MODE = false; // for debug.
    private static String trainFilePath = null;
    private static String testFilePath = null;
    private static int numFeature;
    private static int Kfold = 10;
    private static double threshold = 20.0; // for task11b

    private static ArrayList<Record> trainData = new ArrayList<Record>();
    private static ArrayList<Record> testData = new ArrayList<Record>();

    private static BufferedReader inputReader = null;


    /*** Parameters for Data ***/
    private static ArrayList<Integer> indexOfContiFeature = new ArrayList<Integer>();
    private static ArrayList<Integer> indexOfDisFeature = new ArrayList<Integer>();
    private static int index = 0;
    private static HashMap<String, Integer> label2Index = new HashMap<String, Integer>();  
    /*** End Parameters for Data ***/


    /*** Parameters for Model: DT ***/
    private static double globalBestZScore;
    private static int globalBestNum;
    private static double globalBestCVPrecision;
    private static double bestZScore;
    private static int bestNum;
    private static double bestCVPrecision;
    /*** End Parameters for Model ***/

    /*
     * Parse each line as a Record.
     */
    private static Record readRecord(String line){
        String label = null;
        ArrayList<String> disFeature = new ArrayList<String>();
        ArrayList<Double> conFeature = new ArrayList<Double>();
        String[] words = line.split(",");

        // Read continuous feature
        for(Integer index : indexOfContiFeature)
            conFeature.add(Double.parseDouble(words[index]));

        // Read discrete feature
        for(int i = 0; i < indexOfDisFeature.size() - 1; ++i)
            disFeature.add(words[indexOfDisFeature.get(i)]);
        label = words[indexOfDisFeature.get(indexOfDisFeature.size()-1)];

        return new Record(label, disFeature, conFeature);
    }

    /*
     * Read arff header to know the structure of this dataset.
     */
    private static void readHeader(String line) throws Exception{
        String[] words = line.split(" ");
        if(words[0].equals("@relation")){
            // do nothing
        }
        else if(words[0].equals("@attribute")){
            if(words[2].equals("real"))
                indexOfContiFeature.add(index++);
            else{
                indexOfDisFeature.add(index++);
                String[] cates = words[2].substring(1, words[2].length()-1).split(",");
                label2Index.clear();
                for(int i = 0; i < cates.length; ++i)
                    label2Index.put(cates[i], i);
            }
        }
        else            return;
    }

    /*
     * Read the training data.
     */
    private static void readTrainFile() throws Exception{
        inputReader = new BufferedReader( new InputStreamReader(new FileInputStream(trainFilePath)) );
        String line;
        Record tmp = null;
        boolean headerDone = false;
        while( (line = inputReader.readLine() ) != null ){
            if(line.length() == 0)  continue;
            if(line.equals("@data")){
                headerDone = true;
                continue;
            }
            if( ! headerDone )                readHeader(line);
            else                trainData.add(readRecord(line));
        }
        inputReader.close();
    }

    /*
     * Read the testing data.
     */
    private static void readTestFile() throws Exception{
        inputReader = new BufferedReader( new InputStreamReader(new FileInputStream(testFilePath)) );
        String line;
        Record tmp = null;
        while( (line = inputReader.readLine() ) != null ){
            if(line.length() == 0 || line.startsWith("@"))  continue;
            testData.add(readRecord(line));
        }
        inputReader.close();
    }

    /*
     * Read all necessary file.
     */
    private static void readFile() throws Exception{
        readTrainFile();
        readTestFile(); 
    }

    /*
     * Evaluate the results of validation set, return the precision.
     */
    private static double evaluateValidation(DTreeC45 model, ArrayList<Record> validation){
        double correct = 0.0;
        double wrong = 0.0;
        for(Record r : validation){
            String predict = model.predict(r);
            if(predict.equals(r.label)) correct++;
            else                        wrong ++;
        }
        if(Main.DEV_MODE){
            System.out.printf("after eval val, correct = %f, wrong = %f, prec = %f\n", 
                    correct, wrong, correct / (correct + wrong));
        }
        return correct / (wrong + correct);
    }

    /*
     * Run the k-fold Cross Validation given parameters.
     */
    private static double runCrossValidation(double ZScore, int minNodes, boolean pruned){
        double precision, totalPrecision;

        // 10-fold Cross validation.
        totalPrecision = 0.0;
        for(int k = 0; k < Kfold; k++){
            ArrayList<Record> train = new ArrayList<Record>();
            ArrayList<Record> validation = new ArrayList<Record>();
            for(int i = 0; i < trainData.size(); ++i){
                if( i % Kfold == k )    validation.add(trainData.get(i));
                else                train.add(trainData.get(i));
            }
            precision = evaluateValidation(new DTreeC45(train, ZScore, minNodes, label2Index, pruned), validation);
            totalPrecision += precision;
            if(DEV_MODE){
                System.out.printf("In %d-Fold CV, k=%d, train size = %d, validation size = %d, precision=%f\n", 
                        Kfold, k, train.size(), validation.size(), precision);
            }
        }
        totalPrecision /= Kfold;
        return totalPrecision;
    }

    /*
     * Predict the training data.
     */
    private static double runTraining(DTreeC45 model){
        double precision = evaluateValidation(model, trainData);
        System.out.printf("Training precision = %f\n",  precision);
        return precision;
    }

    /*
     * Tune the paramters by CV to find the best one.
     */
    private static void runTuning(){
        double precision, totalPrecision;
        int maxK, minK;
        double maxZScore, minZScore, step;
        // debug: [0:1:1], k=1
        minK = 1;
        maxK = 5;
        minZScore = 0;
        maxZScore = 2.0;
        step = 0.01;
        // Test 1: [0:10:1], k=10

        globalBestCVPrecision = Double.NEGATIVE_INFINITY;
        for(int k = minK; k <= maxK; k++){
            for(double curZScore = minZScore; curZScore <= maxZScore ; curZScore += step){
                bestCVPrecision = runCrossValidation(curZScore, k, true);
                System.out.printf("\nFor ZScore=%f, minNode=%d range = [%f %f], step size = %f\ncurrent CV precision = %f\n",
                        curZScore, k, minZScore, maxZScore, step, bestCVPrecision);
                if(globalBestCVPrecision < bestCVPrecision){
                    globalBestNum = k;
                    globalBestZScore = curZScore;
                    globalBestCVPrecision = bestCVPrecision;
                    System.out.printf("\nImproved ZScore=%f, minNode=%d range = [%f %f], step size = %f\nbest CV precision = %f\n",
                            curZScore, k, minZScore, maxZScore, step, bestCVPrecision);
                    System.err.printf("\nImproved ZScore=%f, minNode=%d range = [%f %f], step size = %f\nbest CV precision = %f\n",
                            curZScore, k, minZScore, maxZScore, step, bestCVPrecision);
                }
            }
        }
        System.err.printf("\nFor Final ZScore=%f, minNode=%d range = [%f %f], step size = %f\nbest CV precision = %f\n",
                globalBestZScore, globalBestNum, minZScore, maxZScore, step, globalBestCVPrecision);
        System.out.printf("\nFor Final ZScore=%f, minNode=%d range = [%f %f], step size = %f\nbest CV precision = %f\n",
                globalBestZScore, globalBestNum, minZScore, maxZScore, step, globalBestCVPrecision);
    }

    /*
     * Predict the testing data based on default weights.
     */
    private static void runTestData(){
        // Get parameters.
        // best weight learned in training.
        double curZScore;
        int minNode;
        // best for both data a and b, CV precision = 0.822515/0.875 (a/b)
        // CV Precision without pruning: 0.833041/0.925 (a/b) 
        // #Pruned: 31/50, 16/27
        // which means pruning didn't improve CV precision.
        curZScore = 0.0;
        minNode = 1;

        DTreeC45 model = null;
        model = new DTreeC45(trainData, curZScore, minNode, label2Index, false);
        System.out.println(model);
        System.out.printf("\nDoing prediction without pruning for testData...\n");
        model.test(testData);
        System.out.printf("Done prediction with pruning for testData\n\n");
        System.out.printf("\nDoing prediction with pruning for testData...\n");
        model = new DTreeC45(trainData, curZScore, minNode, label2Index, true);
        model.test(testData);
        System.out.printf("Done prediction with pruning for testData\n\n");
        double precision = runCrossValidation(curZScore, minNode, false);
        System.out.printf("\nAfter %d-fold CV without pruning, precision = %f, ZScore = %f, minNode = %d\n", Kfold, precision, curZScore, minNode);
        precision = runCrossValidation(curZScore, minNode, true);
        System.out.printf("After %d-fold CV with pruning, precision = %f, ZScore = %f, minNode = %d\n", Kfold, precision, curZScore, minNode);

    }

    public static void main(String[] args) throws Exception{
        if(args.length != 2){
            System.out.println("Illegal input arguments");
            System.out.println("Usage:\tjava Main <trainFilePath> <testFilePath>");
            System.exit(1);
        }
        trainFilePath = args[0];
        testFilePath = args[1];

        // Read files into memory.
        readFile();
        // Shuffle training data.
        Collections.shuffle(trainData, new Random(3345190));

        // Only used when we want to tune parameters.
        //runTuning();

        // Do prediction for test data.
        runTestData();
    }
}
