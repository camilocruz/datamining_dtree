import java.util.ArrayList;
import java.util.HashMap;
public class Record{
    public String label;  // ground truth
    public ArrayList<String> disFeature;
    public ArrayList<Double> conFeature;
    public static HashMap<String, Integer> label2Index = null;  // Initial in DTreeC45.java

    public Record(String l, ArrayList<String> d, ArrayList<Double> c){
        label = l;
        disFeature = d;
        conFeature = c;
    }

    /*
     *  Get the index of this label.
     */
    public int getLabelInt(){
        return label2Index.get(label);
    }

    @Override
        public String toString(){
            return String.format("label=%s,disF=%s,conF=%s", label, disFeature, conFeature);
        }
}
