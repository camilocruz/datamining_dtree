JAVA=java
JAVAC=javac

INPUTDIRa=../../task11a/attachments/
INPUTDIRb=../../task11b/attachments/

FILEa=ProdSelection.arff
TRAINa=$(INPUTDIRa)train$(FILEa)
TESTa=$(INPUTDIRa)test$(FILEa)

#FILEb=ProdIntro.real.arff
FILEb=ProdIntro.binary.arff
TRAINb=$(INPUTDIRb)train$(FILEb)
TESTb=$(INPUTDIRb)test$(FILEb)

comp:
	$(JAVAC) *.java

clean:
	rm -f *.class

arun:
	$(JAVA) Main $(TRAINa) $(TESTa)
		
brun:
	$(JAVA) Main $(TRAINb) $(TESTb)

zip:
	zip DTC45.zip *.java *.txt Makefile 
