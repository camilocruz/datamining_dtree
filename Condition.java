import java.util.ArrayList;
public class Condition{
    public boolean isDiscrete;
    public int index;
    public String match = null;  // for discrete feature
    public double threshold;  // for continuous feature
    public ArrayList<Condition> children = null;

    // constructor of PARENT of discrete features
    public Condition(int i, ArrayList<String> keys){
        assert keys.size() != 0;
        isDiscrete = true;
        index = i;
        children = new ArrayList<Condition>();
        for(String key : keys)
            children.add(new Condition(i, key));
    }

    // constructor of discrete feature
    public Condition(int i, String key){
        isDiscrete = true;
        index = i;
        match = key;
    }

    // constructor of continuous feature
    public Condition(int i, double t){
        isDiscrete = false;
        index = i;
        threshold = t;
    }
    
    /*
     *  Return whether this record satisfy the condition on this TreeNode.
     */
    public boolean satisfy(Record r){
        if(isDiscrete)
            return r.disFeature.get(index).equals(match);
        else
            return r.conFeature.get(index) < threshold;
    }

    @Override
        public String toString(){
        if(isDiscrete)
            if(children != null)
                return String.format("Condition: isDiscrete=%b, index=%d, size of c = %d, child=%s",
                    isDiscrete, index, children.size(), children);
            else
                return String.format("{%s}", match);
        else
            return String.format("Condition: isDiscrete=%b, index=%d, threshold=%f",
                    isDiscrete, index, threshold);
        }

    /*
     * Below are Accessor functions.
     */
    public ArrayList<Condition> getChildren(){
        return children;
    }

    public boolean getIsDiscrete(){
        return isDiscrete;
    }

}
