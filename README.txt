Team Name: CupCake

Members: Bin He, Camile Cruz Amaluiza, Diwei Shi, Era Dwivedi, Jui-Pin Wang.

Project Description:
    In this project, we implement Decision Tree C4.5 algorithm with Pessimistic Pruning. The default k for Cross Validation is 10, users could modify it at line 15 in Main.java.  The parameters of our Decision Tree are Z-Score and minimun number of instances per TreeNode.  

Usage:
    javac *.java
    java Main <path of training data> <path of testing data>

References:
1.  http://www.cis.temple.edu/~giorgio/cis587/readings/id3-c45.html
2.  http://eric.univ-lyon2.fr/~ricco/cours/slides/en/Methodes_arbres_decision_cart_chaid_c45.pdf
3.  http://www.cs.bc.edu/~alvarez/ML/statPruning.html
4.  http://www-scf.usc.edu/~csci567/21-penalty-methods.pdf
5.  http://www.pmean.com/01/zeroevents.html