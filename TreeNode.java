import java.util.ArrayList;
import java.util.Random;
public class TreeNode{
    private static StringBuilder sb = new StringBuilder();
    private int[] count;  // count of 0/1 labels when considering this node during training.
    private boolean pruned = false;
    private int level = -1;
    private Condition condition = null;
    private ArrayList<TreeNode> children = new ArrayList<TreeNode>();

    private int correct, wrong;
    private double total;
    private double error;
    private double errorBound;
    private double zScore, confidenceFactor;

    public TreeNode(){
    }

    public TreeNode(int _level, int[] _count, double _zScore, double _CF){
        level = _level;
        count = new int[_count.length];
        System.arraycopy(_count, 0, count, 0, _count.length);
        zScore = _zScore;
        confidenceFactor = _CF;

        // Set correct and wrong for pruning.
        int ans = getLabel();
        correct = count[ans];
        for(int i = wrong = 0; i < count.length; ++i){
            total +=count[i]; 
        }
        wrong = (int)total - correct;
        error = (double)wrong / total;
        errorBound = calUpperBound()*total; 
    }

    /*
     *  Add child TreeNode for this TreeNode.
     */
    public void addChild(TreeNode node){
        children.add(node);
    }

    /*
     *  Find the next child node for this record.
     */
    public TreeNode findNext(Record r){
        if(condition.getIsDiscrete()){
            ArrayList<Condition> conds = condition.getChildren();
            for(int i = 0; i < conds.size(); ++i)
                if(conds.get(i).satisfy(r))
                    return children.get(i);
        }
        else{
            if(condition.satisfy(r))    return children.get(0);
            else                        return children.get(1);
        }
        return null; 
    }

    /*
     *  Return whether this record satisfy the condition on this TreeNode.
     */
    public boolean satisfy(Record r){
        return condition.satisfy(r);
    }

    @Override
        public String toString(){
            sb.delete(0, sb.length());
            if(level == -1)    return "#";  // Empty Node
            int total = 0;
            for(int i = 0; i < count.length; total += count[i++]);
            if(pruned)
                sb.append("Pruned ");

            sb.append(String.format("Node: level=%d, #data=%d, pruned=%b, condition=%s, label=%d, w=[",
                        level, total, pruned, condition, getLabel()));
            for(int i = 0; i < count.length; ++i){
                sb.append(count[i]);
                sb.append("\t");
            }
            sb.append("]");
            sb.append(String.format(" c,w=%d,%d, e=%f, bound=%f", correct, wrong, error, errorBound));
            return sb.toString();
        }

    /*
     *  Get the majority label of this node.  Return random label if there is a tie.
     */
    public int getLabel(){
        int best = 0;
        int maxCount = 0;
        for(int i = 0; i < count.length; ++i){
            if( maxCount < count[i] ){
                maxCount = count[i];
            }
        }
        ArrayList<Integer> ans = new ArrayList<Integer>();
        for(int i = 0; i < count.length; ++i){
            if( maxCount == count[i] ){
                ans.add(i);
            }
        }
        if(ans.size() == 1) return ans.get(0);
        else{
            Random rand = new Random();
            return ans.get( rand.nextInt(ans.size()) );
        }
    }

    /*
     * Calculate the upper bound of the confidence interval for this error rate.
     * Ref:
     * 1. http://ocw.mit.edu/courses/sloan-school-of-management/15-097-prediction-machine-learning-and-statistics-spring-2012/lecture-notes/MIT15_097S12_lec08.pdf
    //            return 1.0 - Math.pow(confidenceFactor, 1.0/total);
     * 2. http://www.cs.bc.edu/~alvarez/ML/statPruning.html
     * 3. zero
     * http://www.pmean.com/01/zeroevents.html
     */
    private double calUpperBound(){
        if(error < 1e-10) 
            return 3.0 / total;
        else
            return error + zScore*Math.sqrt(error*(1.0-error)/total);
    }

    /*
     * Below are Accessor functions.
     */
    public double getTotal(){
        return total;
    }

    public double getError(){
        return error;
    }

    public void setError(double e){
        error = e;
    }

    public double getErrorBound(){
        return errorBound;
    }

    public void setErrorBound(double eb){
        errorBound = eb;
    }

    public void setPrune(boolean b){
        pruned = b;
    }

    public ArrayList<TreeNode> getChildren(){
        return children;
    }

    public void setCondition(Condition cond){
        condition = cond;
    }

    public Condition getCondition(){
        return condition;
    }

    public boolean getPruned(){
        return pruned;
    }

    public int getLevel(){
        return level;
    }
}
